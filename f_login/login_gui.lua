
local message_text = "\
Добро пожаловать на ЛЕСРП!\n\
Для игры на нашем сервере, вам необходимо войти или зарегистрироваться!"
--[[Если ты читаешь этот текст, значит ты попал на самый лучший РП проект всех времен\n\
и народов. Если ты пришёл играть к нам, значит тебе надоело все обыденные рп сервера\n\
и ты хочешь чего-то нового, чего раньше ещё не испытывал.\n\
Для входа на сервер, вам необходимо иметь свой аккаунт, если он у вас есть войдите\n\
Если же нет, нажмите кнопку зарегистрироваться внизу окошка"]]

login = {
	window = nil,
    checkbox = nil,
    edit = {},
    label = {},
	button = {}
}

addEventHandler("onClientResourceStart", resourceRoot,
    function( resource )
		if getResourceName( getThisResource() ) ~= getResourceName( resource ) then return end
		login.window = GuiWindow.create( 0.29, 0.25, 0.42, 0.45, "Лагин панель", true )
		login.window:setMovable( false )
		login.window:setSizable( false )
		
		login.label[1] = GuiLabel.create( 0.01, 0.05, 0.98, 0.19, message_text, true, login.window )
		login.label[1]:setFont( "default-bold-small" )
		login.label[1]:setHorizontalAlign( "center", false )
		login.label[1]:setVerticalAlign( "top" )
		
		login.label[2] = GuiLabel.create( 0.02, 0.30, 0.96, 0.05, "Имя пользователя", true, login.window )
		login.label[2]:setFont( "default-bold-small" )
		login.label[2]:setHorizontalAlign( "center", false )
		login.label[2]:setVerticalAlign( "center" )
		
		login.label[3] = GuiLabel.create( 0.02, 0.46, 0.96, 0.05, "Пароль", true, login.window )
		login.label[3]:setFont( "default-bold-small" )
		login.label[3]:setHorizontalAlign( "center", false )
		login.label[3]:setVerticalAlign( "center" )
		
		login.edit[1] = GuiEdit.create( 0.02, 0.37, 0.96, 0.07, localPlayer:getName(), true, login.window )
        login.edit[2] = GuiEdit.create( 0.02, 0.51, 0.96, 0.07, "", true, login.window )
        login.edit[2]:setMasked( true )
		
		login.checkbox = GuiCheckBox.create( 0.71, 0.60, 0.27, 0.05, "Запомнить пароль", false, true, login.window )  

		login.button[1] = GuiButton.create( 0.03, 0.88, 0.44, 0.10, "Войти", true, login.window )	
		login.button[2] = GuiButton.create( 0.54, 0.88, 0.44, 0.10, "Зарегистрироваться", true, login.window )	
		
		login.window:setVisible( false )
		addEventHandler( "onClientGUIClick", login.button[1], loginPlayer, false )
	end
)
