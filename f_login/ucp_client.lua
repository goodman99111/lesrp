
ask = {
	{ text = "Представьте, что вы - петух. Что делать в этом случае?",
		"Ко-ко-ко", "Пойду на турники", "Буду отыгрывать роль петуха", "Буду отыгрывать роль копа с гамбита"
	},
	{ text = "Вы играете за собаку и лежите рядом с курятником, вдруг где-то рядом с вами вы услышали шорох. \
		\nКаковы будут ваши действия в данной ситуации?",
		"Останусь лежать на месте, не царское это дело ходить куда-то",
		"Пойду и проверю нет ли там лисы, что бы прогрызть ей глотку",
		"Я петух, мне плевать",
		"Притворюсь мертвым"
	}
}

ucp = {
	window = nil,
	text = nil,
	answer = {},
	button = nil,
	answers = 1,
	max = 10
}

addEventHandler( "onClientResourceStart", resourceRoot,
    function( resource )
		if getResourceName( getThisResource() ) ~= getResourceName( resource ) then return end
		
		ucp.window = GuiWindow.create( 0.31, 0.31, 0.41, 0.31, "УСП 1/"..ucp.max, true )
		ucp.window:setMovable( false )
		ucp.window:setSizable( false )
		
		ucp.text = GuiLabel.create( 0.05, 0, 0.9, 0.5, ask[1].text, true, ucp.window )
		
		ucp.text:setHorizontalAlign( "center", false )
		ucp.text:setVerticalAlign( "center" )
		
		for i = 1, 4, 1 do 
			ucp.answer[i] = GuiCheckBox.create( 0.03, 0.4 + ( i * 0.08 ), 0.95, 0.06, ask[1][i], false, true, ucp.window )
			addEventHandler( "onClientGUIClick", ucp.answer[i], click_answer )
		end
		ucp.button = GuiButton.create( 0.5, 0.83, 0.5, 0.12, "Дальше", true, ucp.window )
		addEventHandler( "onClientGUIClick", ucp.button, click_button )
	end
)	

function click_answer()
	for i = 1, 4, 1 do 
		if ucp.answer[i] == source then 
			ucp.answer[i]:setSelected( true )
		else ucp.answer[i]:setSelected( false )	end
	end
end

function click_button()
	for i = 1, 4, 1 do 
		if ucp.answer[i]:getSelected() == true then 
			ucp.answers = ucp.answers + 1
			ucp.window:setText( "УСП "..ucp.answers.."/"..ucp.max )
			ucp.answer[i]:setSelected( false )
			
			local rand = math.random( #ask )
			ucp.text:setText( ask[rand].text )
			for u = 1, 4, 1 do 
				ucp.answer[u]:setText( ask[rand][u] )
			end
			
			if ucp.answers == ucp.max then 
				outputDebugString( "ТЕСТ ЗАКОНЧИЛСЯ" )
			end
			return
		end
	end
	outputDebugString( "ВЫ НЕ ВЫБРАЛИ ОТВЕТА!" )
end